import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_pictures(search_term):
    url = "https://api.pexels.com/v1/search?query=" + search_term
    header = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=header)
    data = json.loads(response.content)
    photos = data["photos"]
    return photos
